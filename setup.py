# !/usr/bin/env python

from distutils.core import setup

setup(
    name="radon",
    description="radon script",
    python_requires=">=3.6",
    install_requires=[
        "radon",
    ],
)
