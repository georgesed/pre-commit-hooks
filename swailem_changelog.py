#!/usr/bin/env python3
import re
import sys
import os


version_file = "VERSION"
changelog_file = "CHANGELOG.md"

if not os.path.isfile(version_file):
    print(f"Missing {version_file} file")
    exit(1)
if not os.path.isfile(changelog_file):
    print(f"Missing {changelog_file} file")
    exit(1)

print(f"Comparing {version_file} with {changelog_file}")

version = open(version_file).read().strip()
changelog = open(changelog_file).read()
changelog_version = re.search(r"\d+\.\d(\.\d+)*", changelog).group(0).strip()

print(f"VERSION: {version}")
print(f"CHANGELOG: {changelog_version}")

if version != changelog_version:
    print("VERSION and CHANGELOG don't match")
    sys.exit(1)

print("Verisons match")
sys.exit(0)
